@include('front.includes.head')

<body>



<!-- site wrapper -->
<div class="site-wrapper">

    <div class="main-overlay"></div>

@include('front.includes.header')

   @yield('content')

@include('front.includes.footer')


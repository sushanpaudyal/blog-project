@extends('admin.includes.admin_design')

@section('content')
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">

                <!-- Page Header -->
                <div class="page-header">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="page-title">Change Password</h3>
                        </div>
                    </div>
                </div>
                <!-- /Page Header -->

                @include('admin.includes._message')

                <form method="post" action="{{ route('updatePassword', $admin->id) }}">
                    @csrf
                    <div class="form-group">
                        <label>Old password</label>
                        <input type="password" class="form-control" name="current_password" id="current_password">
                        <p id="correct_password"></p>
                    </div>
                    <div class="form-group">
                        <label>New password</label>
                        <input type="password" class="form-control" name="password" id="password">
                    </div>
                    <div class="form-group">
                        <label>Confirm password</label>
                        <input type="password" class="form-control" name="pass_confirmation" id="pass_confirmation">
                    </div>
                    <div class="submit-section">
                        <button class="btn btn-primary submit-btn">Update Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
           $("#current_password").keyup(function (){
               var current_password = $("#current_password").val();
               $.ajax({
                   headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
                   type: 'post',
                   url: 'check_password',
                   data: {current_password:current_password},
                   success: function (resp){
                       if(resp=="true"){
                           $("#correct_password").text("Current Password Matches").css('color', 'green');
                       } else if (resp == "false"){
                           $("#correct_password").text("Current Password Does Not Matches").css('color', 'red');
                       }
                   }, error: function (resp){
                       alert("Error");
                   }
               });
           });
    </script>
    @endsection

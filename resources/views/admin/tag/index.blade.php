@extends('admin.includes.admin_design')

@section('content')
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Tags</h3>

                    <div class="col-auto float-right ml-auto">
                        <a href="{{ route('tag.add') }}" class="btn add-btn" ><i class="fa fa-plus"></i> Add Tag</a>
                    </div>


                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('adminDashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">View All Tags</li>
                    </ul>



                </div>
            </div>
        </div>
        <!-- /Page Header -->

        @include('admin.includes._message')

        <div class="row">
            <div class="col-sm-12">
                <div class="card mb-0">

                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="category-datatable table table-stripped mb-0" id="tag-datatable">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Tag Name</th>
                                    <th>Tag Slug</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" id="modal-header">
                    <h5 class="modal-title" id="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-body">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('public/backend/assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/backend/assets/js/dataTables.bootstrap4.min.js') }}"></script>


    <script src="{{ asset('public/backend/assets/js/jquery.sweet-alert.custom.js') }}"></script>
    <script src="{{ asset('public/backend/assets/js/sweetalert.min.js') }}"></script>

    <script>
        $("#tag-datatable").DataTable({
            processing: true,
            serverSide: true,
            sorting: true,
            searchable: true,
            responsive: true,
            ajax: "{{ route('table.tag') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'slug', name: 'slug'},
                {data: 'action', name: 'action', orderable: false},
            ]
        });

        $('body').on('click', '.btn-show', function (event){
            event.preventDefault();
            var me = $(this),
                url = me.attr('href'),
                title = me.attr('title')
            $('#modal-title').text(title);
            $.ajax({
                url: url,
                dataType : 'html',
                success: function (response){
                    $("#modal-body").html(response);
                }
            });
            $('#modal').modal('show');
        });

        $('body').on('click', '.btn-delete', function (event){
            event.preventDefault();
            var SITEURL = '{{ URL::to('') }}';
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                },
                function(){
                    window.location.href = SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
        });
    </script>
@endsection

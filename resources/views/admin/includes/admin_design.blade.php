<!DOCTYPE html>
<html lang="en">

@include('admin.includes.head')

<body>
<!-- Main Wrapper -->
<div class="main-wrapper">


@include('admin.includes.header')


@include('admin.includes.sidebar')

<!-- Page Wrapper -->
    <div class="page-wrapper">

       @yield('content')

    </div>
    <!-- /Page Wrapper -->

</div>
<!-- /Main Wrapper -->

<!-- jQuery -->
<script src="{{ asset('public/backend/assets/js/jquery-3.2.1.min.js') }}"></script>

<!-- Bootstrap Core JS -->
<script src="{{ asset('public/backend/assets/js/popper.min.js') }} "></script>
<script src="{{ asset('public/backend/assets/js/bootstrap.min.js') }} "></script>

<!-- Slimscroll JS -->
<script src="{{ asset('public/backend/assets/js/jquery.slimscroll.min.js') }} "></script>

<!-- Chart JS -->
<script src="{{ asset('public/backend/assets/plugins/morris/morris.min.js') }} "></script>
<script src="{{ asset('public/backend/assets/plugins/raphael/raphael.min.js') }} "></script>
<script src="{{ asset('public/backend/assets/js/chart.js') }} "></script>



<!-- Custom JS -->
<script src="{{ asset('public/backend/assets/js/app.js') }} ">  </script>

    @yield('js')

</body>
</html>

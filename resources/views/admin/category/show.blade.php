<table class="table table-hover">
     <tr>
         <th>Category Name</th>
         <td>{{ $model->category_name }}</td>
     </tr>

    <tr>
        <th>Category Slug</th>
        <td>{{ $model->slug }}</td>
    </tr>

    <tr>
        <th>Under Category</th>
        <td>
            @if($model->parent_id == 0)
                Main Category
            @else
                 {{ $model->subCategory->category_name }}
            @endif
        </td>
    </tr>

    <tr>
        <th>Status</th>
        <td>
            @if($model->status == 0)
                <span class="badge badge-danger">In Active</span>
            @else
                <span class="badge badge-success">Active</span>
            @endif
        </td>
    </tr>



    <tr>
        <th>Created At</th>
        <td>
            {{ $model->created_at->diffForHumans() }}
        </td>
    </tr>
</table>

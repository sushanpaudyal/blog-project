@extends('admin.includes.admin_design')

@section('content')
    <!-- Page Content -->
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">

                <!-- Page Header -->
                <div class="page-header">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="page-title">Admin Profile Details</h3>
                        </div>
                    </div>
                </div>
                <!-- /Page Header -->

                @include('admin.includes._message')


                <form method="post" action="{{ route('profileUpdate', $admin->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name"> Name <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="name" id="name" value="{{ $admin->name }}" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">E-Mail Address</label>
                                <input class="form-control" type="email" name="email" id="email" value="{{ $admin->email }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="phone"> Phone Number</label>
                                <input class="form-control" type="text" name="phone" id="phone" value="{{ $admin->phone }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input class="form-control" type="text" name="address" id="address" value="{{ $admin->address }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="info">About You</label>
                                <textarea name="info" id="info" cols="30" rows="4" class="form-control">
                                    {{ $admin->info }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="image">Profile Image</label>
                                <input class="form-control" type="file" name="image" id="image" accept="image/*" onchange="readURL(this);">
                            </div>
                            @if(empty($admin->image))
                                <img src="{{ asset('public/noimage.png') }}" width="100px" id="one">
                            @else
                                <img src="{{ asset('public/uploads/admin/'.$admin->image) }}" width="100px" id="one">
                            @endif
                        </div>
                    </div>

                    <div class="submit-section">
                        <button class="btn btn-primary submit-btn">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Page Content -->
@endsection

@section('js')
    <script type="text/javascript">
           function readURL(input){
               if(input.files && input.files[0]){
                   var reader = new FileReader();
                   reader.onload = function (e){
                       $("#one").attr('src', e.target.result).width(100);
                   }
                   reader.readAsDataURL(input.files[0]);
               }
           }
    </script>
@endsection

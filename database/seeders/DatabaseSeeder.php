<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Admin::insert([
            'name' => "Sushan Paudyal",
            'email' => "sushan.paudyal@gmail.com",
            'password' => bcrypt('password'),
            'phone' => '9803961735',
            'address' => 'Shantinagar, Kathmandu',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}

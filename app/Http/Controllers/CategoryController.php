<?php

namespace App\Http\Controllers;

use App\Exports\CategoryExport;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use DataTables;
use Excel;


class CategoryController extends Controller
{
    // Index Page
    public function index(){
        Session::put('admin_page', 'category');
        return view ('admin.category.index');
    }

    // Add Category
    public function add(){
        $categories = Category::where('parent_id', 0)->get();
        return view ('admin.category.add', compact('categories'));
    }

    // Store Category
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'category_name' => 'required|max:255',
            'order' => 'required',
        ];
        $customMessages = [
            'category_name.required' => 'Category name is required',
            'category_name.max' => 'You are not allowed to enter more than 255 characters',
            'order.required' => 'Please define priority order',
        ];
        $this->validate($request, $rules, $customMessages);

        $slug = Str::slug($data['category_name']);
        $categoryCount = Category::where('slug', $slug)->count();

        if($categoryCount > 0){
            Session::flash('error_message', 'Category name already exists in our database');
            return redirect()->back();
        }

        $category = new Category();
        $category->category_name = ucwords(strtolower($data['category_name']));
        $category->slug = Str::slug($data['category_name']);
        $category->parent_id = $data['parent_id'];
        $category->order = $data['order'];
        if($data['status'] == 1){
            $category->status = 1;
        } else {
            $category->status = 0;
        }
        $category->save();
        Session::flash('success_message', 'Category Has Been Added Successfully');
        return redirect()->back();
    }


    public function dataTable(){
        $model = Category::all();
        return DataTables::of($model)
            ->addColumn('action', function ($model){
                return view ('admin.category._actions', [
                   'model' => $model,
                    'url_show' => route('category.show', $model->id),
                    'url_edit' => route('category.edit', $model->id),
                    'url_delete' => route('category.delete', $model->id),
                ]);
            })
            ->editColumn('parent_id', function ($model){
                if($model->parent_id == 0){
                    return "Main Category";
                } else {
                    return $model->subCategory->category_name;
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function show($id){
        $model = Category::findOrFail($id);
        return view ('admin.category.show', compact('model'));
    }

    public function edit($id){
        $categoryData = Category::findOrFail($id);
        $categories = Category::where('parent_id', 0)->get();
        return view ('admin.category.edit', compact('categoryData', 'categories'));
    }

    // Update Category
    public function update(Request $request, $id){
        $data = $request->all();
        $rules = [
            'category_name' => 'required|max:255',
            'order' => 'required',
        ];
        $customMessages = [
            'category_name.required' => 'Category name is required',
            'category_name.max' => 'You are not allowed to enter more than 255 characters',
            'order.required' => 'Please define priority order',
        ];
        $this->validate($request, $rules, $customMessages);

        $slug = Str::slug($data['category_name']);


        $category = Category::findOrFail($id);
        $category->category_name = ucwords(strtolower($data['category_name']));
        $category->slug = Str::slug($data['category_name']);
        $category->parent_id = $data['parent_id'];
        $category->order = $data['order'];

        if(!empty($data['status'])){
            $category->status = 1;
        } else {
            $category->status = 0;
        }

        $category->save();
        Session::flash('success_message', 'Category Has Been Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){
        $category = Category::findOrFail($id);
        $category->delete();
        DB::table('categories')->where('parent_id', $id)->delete();
        DB::table('posts')->where('category_id', $id)->delete();
        Session::flash('success_message', 'Category Has Been Deleted Successfully');
        return redirect()->back();
    }

    public function exportCategoryExcel(){
        return Excel::download(new CategoryExport, 'category.xlsx');
    }
}

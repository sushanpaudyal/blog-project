<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DataTables;
use Illuminate\Support\Str;

class TagController extends Controller
{
    // Index Page
    public function index(){
        Session::put('admin_page', 'tag');
        return view ('admin.tag.index');
    }

    // Add
    public function add(){
        return view ('admin.tag.add');
    }

    // Store
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'name' => 'required|max:255',
        ];
        $customMessages = [
            'name.required' => 'Tag name is required',
            'name.max' => 'You are not allowed to enter more than 255 characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $tag = new Tag();
        $tag->name = $data['name'];
        $tag->slug = Str::slug($data['name']);
        $tag->save();
        Session::flash('success_message', 'Tag Has Been Added Successfully');
        return redirect()->back();

    }

    public function edit($id){
        $tag = Tag::findOrFail($id);
        return view ('admin.tag.edit')->with(compact('tag'));
    }

    // Store
    public function update(Request $request, $id){
        $data = $request->all();
        $rules = [
            'name' => 'required|max:255',
        ];
        $customMessages = [
            'name.required' => 'Tag name is required',
            'name.max' => 'You are not allowed to enter more than 255 characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $tag = Tag::findOrFail($id);
        $tag->name = $data['name'];
        $tag->slug = Str::slug($data['name']);
        $tag->save();
        Session::flash('success_message', 'Tag Has Been Updated Successfully');
        return redirect()->back();

    }

    public function delete($id){
        $tag = Tag::where('id', $id)->delete();
        Session::flash('success_message', 'Tag Has Been Deleted Successfully');
        return redirect()->back();
    }

    public function dataTable(){
        $model = Tag::latest()->get();
        return DataTables::of($model)
            ->addColumn('action', function ($model){
                return view ('admin.tag._actions', [
                    'model' => $model,
                    'url_edit' => route('tag.edit', $model->id),
                    'url_delete' => route('tag.delete', $model->id),
                ]);
            })
            ->editColumn('parent_id', function ($model){
                if($model->parent_id == 0){
                    return "Main Category";
                } else {
                    return $model->subCategory->category_name;
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FrontEndController extends Controller
{
    // Index Page
    public function index(){
        $latest_post = Post::where(['status' => 'Published'])->latest()->take(1)->get();
        $popular_posts = Post::where(['status' => 'Published'])->orderBy('view_count', 'DESC')->take(4)->get();
        $latest_posts = Post::where(['status' => 'Published'])->latest()->take(4)->get();
        $categories = Category::where('status', 1)->where('parent_id', 0)->get();
        return view ('front.index', compact('latest_post', 'popular_posts', 'latest_posts', 'categories'));
    }

    // Category Single Page
    public function categorySingle($slug){
        $category = Category::where('slug', $slug)->first();
        $posts = Post::where(['status' => 'Published'])->where('category_id', $category->id)->latest()->paginate(4);
        $popular_posts = Post::where(['status' => 'Published'])->orderBy('view_count', 'DESC')->take(4)->get();
        $categories = Category::where('status', 1)->where('id', '!=', $category->id)->latest()->get();
        $tags = Tag::all();

        return view ('front.categorySingle', compact('category', 'posts', 'popular_posts', 'categories', 'tags'));
    }

    // Post Single
    public function postSingle($slug){
        $post = Post::where('slug', $slug)->first();

        $postKey = 'post_' . $post->id;
        if(!Session::has($postKey)){
            $post->increment('view_count');
            Session::put($postKey, 1);
        }

        return view ('front.postSingle', compact('post'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Image;

class AdminProfileController extends Controller
{
    // Admin Profile
    public function profile(){
        $admin = Auth::guard('admin')->user();
        return view ('admin.profile', compact('admin'));
    }

    // Admin Profile Update
    public function profileUpdate(Request $request, $id){
        $data = $request->all();
        $rules = [
            'email' => 'required|email|max:255',
            'name' => 'required|max:255'
        ];
        $customMessages = [
            'name.required' => 'Name is required',
            'name.max' => 'You are not allowed to enter more than 255 characters',
            'email.required' => 'You Must Enter email address',
            'email.email' => 'Please enter a valid email address',
            'email.max' => 'You are not allowed to enter more than 255 characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $profile = Admin::findOrFail($id);
        $profile->name = $data['name'];
        $profile->email = $data['email'];
        $profile->phone = $data['phone'];
        $profile->address = $data['address'];
        $profile->info = $data['info'];

        $random = rand(111, 9999999999);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random.'.'.$extension;
                $image_path = 'public/uploads/admin/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $profile->image = $filename;
            }
        }


        $profile->save();
        Session::flash('success_message', 'Admin Profile Has Been Updated Successfully');
        return redirect()->back();

    }

    // Change Password
    public function changePassword(){
        $admin = Admin::where('email', Auth::guard('admin')->user()->email)->first();
        return view ('admin.change_password', compact('admin'));
    }

    // Check Current Password
    public function chkUserPassword(Request $request){
        $data = $request->all();
        $current_password = $data['current_password'];
        $user_id = Auth::guard('admin')->user()->id;
        $check_password = Admin::where('id', $user_id)->first();
        if (Hash::check($current_password, $check_password->password)){
            return "true"; die;
        } else {
            return "false"; die;
        }
    }

    // Update Admin password
    public function updatePassword(Request $request, $id){
        $data = $request->all();
        $validateData = $request->validate([
            'current_password' => 'required|max:255',
            'password' => 'min:6',
            'pass_confirmation' => 'required_with:password|same:password|min:6',
        ]);
        $admin = Admin::where('email', Auth::guard('admin')->user()->email)->first();
        $current_password = $admin->password;
        if(Hash::check($data['current_password'], $current_password)){
            $admin->password = bcrypt($data['password']);
            $admin->save();
            Session::flash('success_message', 'Your Password Has Been Changed Successfully');
            return redirect()->back();
        } else {
            Session::flash('error_message', 'Your Current Password Does Not Match with our Database');
            return redirect()->back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Image;
use DataTables;

class PostController extends Controller
{
    // Index Page
    public function index(){
        Session::put('admin_page', 'post');
        return view ('admin.post.index');
    }

    // Add Post
    public function add(){
        $categories = Category::where(['parent_id' => 0])->get();
        $categories_dropdown = "<option selected disabled> Select Category </option>";
        foreach ($categories as $cat){
            $categories_dropdown .= "<option value=' ". $cat->id ." '> ". $cat->category_name ." </option>";
            $sub_categories = Category::where('parent_id', $cat->id)->get();
            foreach ($sub_categories as $sub_cat){
                $categories_dropdown .= "<option value='". $sub_cat->id ."'>  &nbsp; &nbsp; ---- ". $sub_cat->category_name ."</option>";
            }
        }

        $tags = Tag::all();
        return view ('admin.post.add', compact('categories_dropdown', 'tags'));
    }

    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'post_title' => 'required|max:255',
        ];
        $customMessages = [
            'post_title.required' => 'Post Title is required',
            'post_title.max' => 'You are not allowed to enter more than 255 characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $post = new Post();
        $post->post_title = $data['post_title'];
        $post->slug = Str::slug($data['post_title']);
        $post->category_id = $data['category_id'];
        if(!empty($data['post_content'])){
            $post->post_content = $data['post_content'];
        } else {
            $post->post_content = "";
        }

        $slug = Str::slug($data['post_title']);
        $random = rand(111, 9999999999);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $slug.'.'.$random.'.'.$extension;
                $image_path = 'public/uploads/posts/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $post->image = $filename;
            }
        }
        if(!empty($data['status'])){
            $post->status = "Published";
        } else {
            $post->status = "Draft";

        }

        $post->view_count = 0;
        $post->seo_title = $data['seo_title'];
        $post->seo_subtitle = $data['seo_subtitle'];
        $post->seo_keywords = $data['seo_keywords'];
        $post->seo_description = $data['seo_description'];

        $admin_id = Auth::guard('admin')->user()->id;
        $post->admin_id = $admin_id;


        $tags = $data['tag_id'];
        $post->save();
        $post->tags()->attach($tags);
        Session::flash('success_message', 'Post Has Been Added Successfully');
        return redirect()->back();

    }

    public function dataTable(){
        $model = Post::latest()->get();
        return DataTables::of($model)
            ->addColumn('action', function ($model){
                return view ('admin.post._actions', [
                    'model' => $model,
                    'url_show' => route('post.show', $model->id),
                    'url_edit' => route('post.edit', $model->id),
                    'url_delete' => route('post.delete', $model->id),
                ]);
            })
            ->addIndexColumn()
            ->editColumn('category_id', function ($model){
                return $model->category->category_name;
            })
            ->editColumn('post_content', function ($model){
                $content = Str::limit(strip_tags($model->post_content), 300, '...');
                return $content;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function show($id){
        $model = Post::findOrFail($id);
        return view ('admin.post.show', compact('model'));
    }

    public function edit($id){
        $post = Post::findOrFail($id);

        $categories = Category::where(['parent_id' => 0])->get();
        $categories_dropdown = "<option selected disabled> Select Category </option>";
        foreach ($categories as $cat){

            if($cat->id == $post->category_id){
                $selected = "selected";
            } else {
                $selected = "";
            }

            $categories_dropdown .= "<option value=' ". $cat->id ."' ".$selected."> ". $cat->category_name ." </option>";
            $sub_categories = Category::where('parent_id', $cat->id)->get();
            foreach ($sub_categories as $sub_cat){
                if($sub_cat->id == $post->category_id){
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $categories_dropdown .= "<option value='". $sub_cat->id ."' ".$selected.">  &nbsp; &nbsp; ---- ". $sub_cat->category_name ."</option>";
            }
        }
        $tags = Tag::orderBy('name')->get();
        $post_tag = $post->tags()->pluck('tag_id')->toArray();


        return view ('admin.post.edit', compact('post', 'categories', 'categories_dropdown', 'tags', 'post_tag'));
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $rules = [
            'post_title' => 'required|max:255',
        ];
        $customMessages = [
            'post_title.required' => 'Post Title is required',
            'post_title.max' => 'You are not allowed to enter more than 255 characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $post = Post::findOrFail($id);
        $post->post_title = $data['post_title'];
        $post->slug = Str::slug($data['post_title']);
        $post->category_id = $data['category_id'];
        if(!empty($data['post_content'])){
            $post->post_content = $data['post_content'];
        } else {
            $post->post_content = "";
        }

        $slug = Str::slug($data['post_title']);
        $random = rand(111, 9999999999);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $slug.'.'.$random.'.'.$extension;
                $image_path = 'public/uploads/posts/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $post->image = $filename;
            }
        }
        if(!empty($data['status'])){
            $post->status = "Published";
        } else {
            $post->status = "Draft";

        }

        $post->view_count = 0;
        $post->seo_title = $data['seo_title'];
        $post->seo_subtitle = $data['seo_subtitle'];
        $post->seo_keywords = $data['seo_keywords'];
        $post->seo_description = $data['seo_description'];

        $admin_id = Auth::guard('admin')->user()->id;
        $post->admin_id = $admin_id;

        $tags = $data['tag_id'];
        $post->save();
        $post->tags()->sync($tags);
        Session::flash('success_message', 'Post Has Been Updated Successfully');
        return redirect()->back();

    }

    public function delete($id){
        $post = Post::findOrFail($id);
        $post->delete();
        $image_path = 'public/uploads/posts/';
        if(!empty($post->image)){
            if(file_exists($image_path.$post->image)){
                unlink($image_path.$post->image);
            }
        }
        Session::flash('success_message', 'Post Has Been Deleted Successfully');
        return redirect()->back();
    }
}

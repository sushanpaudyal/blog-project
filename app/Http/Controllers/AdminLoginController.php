<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class AdminLoginController extends Controller
{
    // Admin Login
    public function login(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $rules = [
                'email' => 'required|email|max:255',
                'password' => 'required'
            ];
            $customMessages = [
              'email.required' => 'You Must Enter email address',
              'email.email' => 'Please enter a valid email address',
              'email.max' => 'You are not allowed to enter more than 255 characters',
              'password.required' => 'Password is required',
            ];
            $this->validate($request, $rules, $customMessages);

            if (Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password']])){
                return  redirect()->route('adminDashboard');
            } else {
                Session::flash('error_message', 'Invalid Username or Password');
                return redirect()->route('adminLogin');
            }
        }

        if(Auth::guard('admin')->check()){
            return  redirect()->route('adminDashboard');
        } else {
            return view ('admin.login');
        }

    }

    // Admin Dashboard
    public function dashboard(){
        Session::put('admin_page', 'dashboard');
        return view ('admin.dashboard');
    }

    // Logout
    public function logout(){
        Auth::guard('admin')->logout();
        Session::flash('success_message', 'Logout Successful');
        return redirect()->route('adminLogin');
    }

    // Reset Password
    public function forgetPassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $rules = [
                'email' => 'required|email',
            ];
            $customMessages = [
                'email.required' => 'You Must Enter email address',
                'email.email' => 'Please enter a valid email address',
            ];
            $this->validate($request, $rules, $customMessages);

            $adminCount = Admin::where('email', $data['email'])->count();
            if($adminCount == 0){
                return redirect()->back()->with('error_message', 'Email Address Does Not Exist in Our Database');
            }
            // Get Admin Details
            $adminDetails = Admin::where('email', $data['email'])->first();
            // Generate Random password
            $random_password = Str::random(10);
            // encode password
            $new_password = bcrypt($random_password);
            // Update password
            Admin::where('email', $data['email'])->update(['password' => $new_password]);

            // Send Forget password email
            $email = $data['email'];
            $name = $adminDetails->name;
            $messageData = ['email' => $email, 'password' => $random_password, 'name' => $name];
            Mail::send('emails.resetpassword', $messageData, function ($message) use ($email){
                $message->to($email)->subject('New Password From Blog Project');
            });
            return redirect()->route('adminLogin')->with('success_message', 'Please check your email for a new password');


        }
        return view ('admin.reset');
    }
}
